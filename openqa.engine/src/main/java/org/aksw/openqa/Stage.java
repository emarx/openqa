package org.aksw.openqa;

import java.util.Arrays;
import java.util.List;

import org.aksw.openqa.component.IComponent;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.manager.plugin.PluginManager;

public abstract class Stage implements IComponent {
	String id;
	List<String> componentIDs;
	List<Stage> stages;
	
	public abstract IResultMapList<? extends IResultMap> process(
			List<? extends IParamMap> params, 
			ServiceProvider services, 
			IContext context, 
			PluginManager pluginManager);
	
	public List<String> getComponentIDs() {
		return componentIDs;
	}
	
	public void setComponentIDs(List<String> componentIDs) {
		this.componentIDs = componentIDs;
	}
	
	public void setComponentIDs(String... componentIDs) {
		this.componentIDs = Arrays.asList(componentIDs);
	}
	
	public void setSubStages(List<Stage> stages) {
		this.stages = stages;
	}
	
	public void setComponentIDs(Stage... stages) {
		this.stages = Arrays.asList(stages);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
