package org.aksw.openqa.component.context;

import java.util.HashMap;
import java.util.Map;

import org.aksw.openqa.component.AbstractPlugin;
import org.aksw.openqa.component.ISpecializedPluginVisitor;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public abstract class AbstractContext extends AbstractPlugin implements IContext {	

	Map<String, Object> configParams = new HashMap<String, Object>();

	public AbstractContext(Map<String, Object> params) {
		super(params);
	}
	
	public void accept(ISpecializedPluginVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public Object getContextParam(String param) {
		return configParams.get(param);
	}
	
	@Override
	public void setContextParam(String param, Object value) {
		configParams.put(param, value);
	}
}
