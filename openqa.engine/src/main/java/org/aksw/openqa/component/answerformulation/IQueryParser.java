package org.aksw.openqa.component.answerformulation;

import org.aksw.openqa.component.IPluginProcess;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public interface IQueryParser extends IPluginProcess {
}