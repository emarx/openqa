package org.aksw.openqa.component.param;

import java.util.ArrayList;
import java.util.List;

import org.aksw.openqa.component.IComponent;

public class ResultMapListMetaInfo<E extends IResultMap> extends ArrayList<E> implements IResultMapListMetaInfo<E> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4375235578274817988L;
	protected IComponent source;
	protected IParamMap inputParam;
	protected List<? extends IParamMap> inputParams;

	@Override
	public IParamMap getInputParam() {
		return inputParam;
	}
	
	public void setInputParam(IParamMap inputParam) {
		this.inputParam = inputParam;
	}

	@Override
	public IComponent getSource() {
		return source;
	}
	
	public void setSource(IComponent source) {
		this.source = source;
	}

	@Override
	public List<? extends IParamMap> getInputParams() {
		return inputParams;
	}
	
	public void setInputParams(List<? extends IParamMap> inputParams) {
		this.inputParams = inputParams;
	}	

	@Override
	public void printStackTrace() {
		PrintStackTraceResultVisitor ps = new PrintStackTraceResultVisitor();
		accept(ps);
	}

	@Override
	public String toJSON() {
		JSONResultVisitor rv = new JSONResultVisitor();
		accept(rv);
		return rv.getJSON();
	}

	@Override
	public void accept(IMapVisitor visitor) {
		visitor.visit(this);		
	}
}
