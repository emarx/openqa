package org.aksw.openqa.component.retriever;

public interface IEncoder<T,L> {
	public T encode(L object);
	public L decode(T object);
}
