package org.aksw.openqa.component.service;

import org.aksw.openqa.component.IPlugin;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public interface IService extends IPlugin {
}
