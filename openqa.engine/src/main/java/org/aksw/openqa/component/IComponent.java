package org.aksw.openqa.component;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public interface IComponent {

	/**
	 * Return the id of the component.
	 * 
	 * @return the id of the IComponent id.
	 */
	public String getId();
}
