package org.aksw.openqa.component.service;

import java.util.Map;

import org.aksw.openqa.component.AbstractPlugin;
import org.aksw.openqa.component.ISpecializedPluginVisitor;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public abstract class AbstractService extends AbstractPlugin implements IService {

	public AbstractService(Map<String, Object> params) {
		super(params);
	}
	
	public void accept(ISpecializedPluginVisitor visitor) {
		visitor.visit(this);
	}
	
}