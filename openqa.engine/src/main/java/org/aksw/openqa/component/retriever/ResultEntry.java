package org.aksw.openqa.component.retriever;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class ResultEntry {
	private Map<String, PropertyEntry> properties = new HashMap<String, PropertyEntry>();
	
	public PropertyEntry getProperty(String label) {
		return properties.get(label);
	}
	
	public Object getPropertyValue(String label) {
		PropertyEntry pe = getProperty(label);
		if(pe != null) {
			return pe.getValue();
		}
		return null;
	}
	
	public <T> T getPropertyValue(String label, Class<T> clazz) {
		PropertyEntry pe = getProperty(label);
		if(pe != null) {
			return clazz.cast(pe.getValue());
		}
		return null;
	}
	
	public void addProperty(String label, Object value) {
		PropertyEntry propertyEntry = new PropertyEntry();
		propertyEntry.setType(value.getClass());
		propertyEntry.setValue(value);
		properties.put(label, propertyEntry);
	}
	
	public boolean contains(String propertyLabel) {
		return properties.containsKey(propertyLabel);
	}
	
	public Set<Entry<String, PropertyEntry>> listProperties() {
		return properties.entrySet();
	}
}
