package org.aksw.openqa.component;

import java.util.Map;

import org.aksw.openqa.component.param.IMap;

/**
 * Plug-in interface
 * 
 * @author emarx
 */
public interface IPlugin extends IMap, IComponent {
	
	public String getVersion();
	
	public String getLabel();
	
	public boolean isActive();
	public void setActive(boolean active);
	
	public void shutdown();
	
	public void setProperties(Map<String, Object> params);
	
	public String getAuthor();
	
	public String getWebsite();
	
	public String getInput();
	
	public String getOutput();
	
	public String getContact();
	
	public String getDescription();
	
	public String getLicense();

	public void accept(IPluginVisitor visitor);
	
	public void accept(ISpecializedPluginVisitor visitor);

	public String getAPI();

}
