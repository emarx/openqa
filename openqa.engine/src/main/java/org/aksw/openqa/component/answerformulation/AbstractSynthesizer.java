package org.aksw.openqa.component.answerformulation;

import java.io.IOException;
import java.util.Map;

import org.aksw.openqa.component.AbstractQFProcessor;
import org.aksw.openqa.component.ISpecializedPluginVisitor;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public abstract class AbstractSynthesizer extends AbstractQFProcessor implements ISynthesizer {

	public AbstractSynthesizer(Map<String, Object> params)
			throws IOException {
		super(params);
	}
	
	public void accept(ISpecializedPluginVisitor visitor) {
		visitor.visit(this);
	}
}
