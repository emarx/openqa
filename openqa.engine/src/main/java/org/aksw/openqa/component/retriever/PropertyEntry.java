package org.aksw.openqa.component.retriever;

public class PropertyEntry {

	private Class<?> type;
	private Object value;
	
	public Class<?> getType() {
		return type;
	}
	public void setType(Class<?> type) {
		this.type = type;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
}
