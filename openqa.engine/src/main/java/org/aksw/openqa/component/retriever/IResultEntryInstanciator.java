package org.aksw.openqa.component.retriever;

public interface IResultEntryInstanciator <T> extends IRetrieverEntryInstanciator<T, ResultEntry> {
	T newInstance(ResultEntry entry);
	boolean stop();
}
