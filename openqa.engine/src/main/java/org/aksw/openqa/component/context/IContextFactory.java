package org.aksw.openqa.component.context;

import org.aksw.openqa.component.IPluginFactorySpi;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public interface IContextFactory extends IPluginFactorySpi<IContext> {
}
