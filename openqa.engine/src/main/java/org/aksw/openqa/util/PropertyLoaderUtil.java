package org.aksw.openqa.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertyLoaderUtil {
	
	private static Logger logger = Logger.getLogger(PropertyLoaderUtil.class);
	
	public static Map<String, Object> getProperties(Class<?> clazz, String filePath) {
		Map<String, Object> params = null;
		try (InputStream inStream = clazz.getResourceAsStream(filePath)) { // looking for component params file directory
			if(inStream != null) {
				params = loadParams(inStream); // loading properties
			}
		} catch (Exception e) {
			logger.error("Error initializing component params:" + clazz.getName(), e);
		}
		return params;
	}
	
	public static Map<String, Object> getPropertiesWithWhiteSpaceFromFile(String filePath) {
		Map<String, Object> params = null;
		try {
			File f = new File(filePath);
			if(f.exists()) {
				try(FileReader fileReader = new FileReader(f)) { // looking for component params file directory
					if(fileReader != null) {
						params = loadParamsWithWhiteSpace(fileReader); // loading properties
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error loading params:" + filePath, e);
		}
		return params;
	}
	
	public static Map<String, Object> getPropertiesFromFile(String filePath) {
		Map<String, Object> params = null;
		try {
			File f = new File(filePath);
			if(f.exists()) {
				try(InputStream inStream = new FileInputStream(filePath)) { // looking for component params file directory					 
					if(inStream != null) {
						params = loadParams(inStream); // loading properties
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error loading params:" + filePath, e);
		}
		return params;
	}
	
	protected static Map<String, Object> loadParamsWithWhiteSpace(FileReader fileReader) throws IOException {
		Map<String, Object> params = new HashMap<String, Object>();
		if(fileReader != null) {
			try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					String[] attr = line.split("=");
					params.put(attr[0].trim(), attr[1].trim());
				}
			}
		}
		return params;
	}

	protected static Map<String, Object> loadParams(InputStream is) throws IOException {
		Map<String, Object> params = new HashMap<String, Object>();
 		Properties prop = new Properties();
		if(is != null) {
			prop.load(is);
			for(Object key : prop.keySet()) {
				params.put((String) key, prop.get(key));
			}				
		}
		return params;
	}
}
