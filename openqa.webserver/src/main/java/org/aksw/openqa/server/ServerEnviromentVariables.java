package org.aksw.openqa.server;

import javax.servlet.ServletContext;

import org.aksw.openqa.SystemVariables;
import org.aksw.openqa.manager.plugin.PluginManager;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class ServerEnviromentVariables extends SystemVariables {
	public static final String SERVLET_CONTEXT_PARAM = "SERVLET_CONTEXT_PARAM";
	public static final String SERVLET_PREDEFINED_QUERY_LIST_PARAM = "SERVLET_PREDEFINED_QUERY_LIST_PARAM";
	public static final String PLUGGIN_DIR = "PLUGIN_DIR";
	public static final String PLUGIN_MANAGER_POOL = "PLUGIN_MANAGER_POOL";
	public static final String CONTEXT_DIR = "CONTEXT_DIR";
	public static final String ClASS_LOADER = "ClASS_LOADER";
	public static final String APP_CONTEXT = "APP_CONTEXT";
	public static final String APP_CACHE = "CACHE";
	public static final String PLUGIN_MANAGER_POOL_INSTANCIATOR = "PLUGIN_MANAGER_POOL_INSTANCIATOR";
	
	private static ServerEnviromentVariables systemVariables;
	
	public static ServerEnviromentVariables getInstance() {
		if(systemVariables == null) {
			systemVariables = new ServerEnviromentVariables();
		}
		return systemVariables;
	}
	
	public ServletContext getServletContext() {
		return getParam(SERVLET_CONTEXT_PARAM, ServletContext.class);
	}
	
	public static PluginManager getPluginManager() {
		return ((PluginManager)(ServerEnviromentVariables.getInstance().getParam(SystemVariables.PLUGIN_MANAGER)));
	}
}
